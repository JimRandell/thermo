import React from 'react'
import ArrowDropUpRoundedIcon from '@material-ui/icons/ArrowDropUpRounded';
import ArrowDropDownRoundedIcon from '@material-ui/icons/ArrowDropDownRounded';
import Panel from './components/Panel'


interface Props {}

interface State {
		curTemp: number,
		flagIsPowerSaveMode:boolean,
}


class App extends React.Component <Props, State >  {


			constructor(props: Props) {
				super(props)
				this.state = {
					curTemp: 20,
					flagIsPowerSaveMode: false,
				}
			}


	// switchOnChange:any

			updateCurTemp = (curTemp: number) => {this.setState({ 'curTemp' :curTemp })}
			// powerSaveOn:boolean = this.state.flagIsPowerSaveMode
			togglePowerSave = () => this.setState({  'flagIsPowerSaveMode': !this.state.flagIsPowerSaveMode })

			powerSaveOn = ()=> this.state.curTemp > 23 && this.state.flagIsPowerSaveMode


			updatePowerSave = () => {
			if(this.state.curTemp > 23 && !this.state.flagIsPowerSaveMode)
				{this.togglePowerSave()
				this.updateCurTemp(24)}
			else this.togglePowerSave()
			}


			doNotChange = (val:number) =>	(this.state.curTemp > (24 - val) && this.state.flagIsPowerSaveMode) ||
							this.state.curTemp < (8 - val) ||
							this.state.curTemp > (32 - val)


			onIncrement = (val:number) =>{
				 if(!this.doNotChange(val))
				 this.updateCurTemp(this.state.curTemp + val)

			}




		evalColor = () => {
			let	temp:number  = this.state.curTemp
			let dColor:string = ''
				switch (true) {
					case (temp<18): dColor = 'lightskyblue'; break;
					case (temp>17 && temp<24): dColor = 'springgreen'; break;
					case (temp>23): dColor = 'tomato'; break;
					default:alert("none"); break;
				}
				let displayColor:string = ''
				return  displayColor = dColor
		}








	render() {

		 return <Panel
			switchOnChange = {() =>{this.updatePowerSave()}}
			switchChecked={this.state.flagIsPowerSaveMode}
			switchColor='primary'
			button1Icon= {<ArrowDropUpRoundedIcon  style={{fontSize:100}} />}
			button1Id='button1'
			button1Name='button1'
			button1Size='small'
			button1Color='primary'
			button1Variant=''
			button1onClick={()=>this.onIncrement(1)}
			button2Icon= {<ArrowDropDownRoundedIcon  style={{fontSize:100}} />}
			button2Id='button2'
			button2Name='button2'
			button2Size='small'
			button2Color='primary'
			button2Variant=""
			button2onClick={()=>this.onIncrement(-1)}

			// onChangeFld1={this.adjustTemp}
			fld1Variant='filled'
			fld1Id='adjust'
			fld1Label=''
			fld1Value={this.state.curTemp}
			fld1Margin='dense'
			fld1Name='adjust'
			fld1Type='login'
			fld1Color={this.evalColor()}

		/>
	}
}
export default  App