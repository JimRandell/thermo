import React from 'react'
import { FormControlLabel, Grid,  IconButton, InputBase, Paper, Switch } from '@material-ui/core'
import PropTypes from 'prop-types'


export interface Props {
	button1Color: any,
	button1Icon: any,
	button1Id: string,
	button1Name: string,
	button1Size: any,
	button1Variant: any,
	button1onClick: any,
	button2Color: any,
	button2Icon: any,
	button2Id: string,
	button2Name: string,
	button2Size: any,
	button2Variant: any,
	button2onClick: any,
	fld1Color: any,
	fld1Id:string,
	fld1Label:string,
	fld1Margin:string,
	fld1Name:string,
	fld1Type:string,
	fld1Value: number,
	fld1Variant:any,
	switchChecked: boolean,
	switchColor: any,
	switchOnChange: any,
}



interface State {}


export class Panel extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props)
		this.state = {}
	}

	render() {
		return (
			<>
				<Paper
					style={{
						display: 'flex',
						color: 'red',
						textAlignLast: 'center',
						flexWrap: 'wrap',
						margin: 30,
						padding: 30,
						width: 200,
					}}
				>
					<Grid
						container
						direction="column"
						style={{ paddingTop: 5 }}
						justify="space-evenly"
						alignItems="center"
					>
						<IconButton
							onClick={this.props.button1onClick}
							id={this.props.button1Id}
							name={this.props.button1Name}
							color={this.props.button1Color}
							size={this.props.button1Size}
						>
							{this.props.button1Icon}
						</IconButton>

						<InputBase
							style={{
								color: this.props.fld1Color,
								boxShadow: 'inset',
								border: 'inset',
								backgroundColor: 'darkslategrey',
							}}
							type="number"
							id="quantity"
							name="quantity"
							value={this.props.fld1Value}
						/>

						<IconButton
							onClick={this.props.button2onClick}
							id={this.props.button2Id}
							name={this.props.button2Name}
							color={this.props.button2Color}
							size={this.props.button2Size}
						>
							{this.props.button2Icon}
						</IconButton>

						<FormControlLabel
							control={
								<Switch
									checked={this.props.switchChecked}
									onChange={this.props.switchOnChange}
									color={this.props.switchColor}
								/>
							}
							label="Power Save Mode"
						/>
					</Grid>
				</Paper>
			</>
		)
	}
}


export default Panel
